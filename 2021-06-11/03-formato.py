# Formato de cadenas

# %d = Numero entero
# %f = Numero decimal (punto flotante)
# %s = Cadena de caracteres

a = 20
b = 30.5539847583
c = 'texto'

mensaje = 'La frase tiene %s palabras. %.6f, %d' % (c, b, a)

print(mensaje)