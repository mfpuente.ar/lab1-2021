fruta = 'manzana'

# Primera letra
primera = fruta[0]
print(primera)

# Ultima letra
ultima = fruta[-1]
print(ultima)

# Rebanado de una cadena
# cadena[m:n] => m se incluye, n no se incluye
subcadena = fruta[2:6]
print(subcadena)

frase = 'Una frase para probar'
#primera = frase[0:3]
primera = frase[:3]
segunda = frase[4:9]
tercera = frase[10:14]
#cuarta = frase[15:21]
#cuarta = frase[15:]
cuarta = frase[-6:]
print(primera)
print(segunda)
print(tercera)
print(cuarta)

# Rebanado con paso
# cadena[m:n:p] => m se incluye, n no se incluye, paso
print(frase[0:5:2])

# Cadena completa
print(frase[:])

# Cadena completa pero invertida
print(frase[::-1])

