# Suma
print('suma:', 1+4)

# Resta
print('resta:', 4-2)

# Multiplicacion
print('multiplicacion:', 3*3)

# Division
print('division:', 5/3)

# Potencia
print('potencia:', 2**3)

# Resto o modulo
print('resto:', 5%3)

# Division divida o division entera
print('divison dividida:', 5//3)