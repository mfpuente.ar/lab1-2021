# Comillas simples o dobles
cadena1 = "un 'texto'"
cadena2 = 'otro "texto"'

print(cadena1)
print(cadena2)


# Multiples lineas
cadena3 = '''Texto en
varias
lineas'''

cadena4 = """Otro texto
en varias
lineas"""

print(cadena3)
print(cadena4)