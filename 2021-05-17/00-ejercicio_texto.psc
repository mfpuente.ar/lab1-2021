Algoritmo ejercicio_texto
	// Cargar un arreglo de 5 elementos con palabras
	// Mostrar la longitud de cada palabra
	// Mostrar la palabra mas larga y la mas corta
	dimension palabras[5]
	
	// Cargar arreglo
	para i=1 hasta 5
		leer palabras[i]
	FinPara
	
	// Mostrar longitudes
	para i=1 hasta 5
		Escribir palabras[i] ' tiene ' longitud(palabras[i]) ' letras'
	FinPara
	
	larga = 1
	corta = 1
	
	// Palabra mas larga y palabra mas corta
	para i = 2 hasta 5
		si (longitud(palabras[larga]) < longitud(palabras[i])) Entonces
			larga = i
		FinSi
		si (longitud(palabras[corta]) > longitud(palabras[i])) Entonces
			corta = i
		FinSi
	FinPara
	
	escribir 'La palabra mas larga es ' palabras[larga] ' con ' longitud(palabras[larga]) ' letras'
	escribir 'La palabra mas corta es ' palabras[corta] ' con ' longitud(palabras[corta]) ' letras'
	
FinAlgoritmo
