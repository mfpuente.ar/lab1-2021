Algoritmo ejercicio_texto
	// Cargar un arreglo de 5 elementos con palabras
	// Ordenar el arreglo desde la palabra mas corta a la mas larga
	dimension palabras[5]
	
	// Cargar arreglo
	para i=1 hasta 5
		leer palabras[i]
	FinPara
	
	// Ordenar
	para i = 1 hasta 4
		para j = i+1 hasta 5
			si (longitud(palabras[i]) > longitud(palabras[j])) Entonces
				aux = palabras[i]
				palabras[i] = palabras[j]
				palabras[j] = aux
			FinSi
		FinPara
	FinPara
	
	// Mostrar arreglo
	para i=1 hasta 5
		escribir palabras[i]
	FinPara
FinAlgoritmo
