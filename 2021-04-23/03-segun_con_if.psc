Algoritmo segun_con_if
	Escribir 'Ingrese un numero: '
	Leer a
	Si a=0 Entonces
		Escribir 'cero'
	SiNo
		Si a=1 Entonces
			Escribir 'uno'
		SiNo
			Si a=2 Entonces
				Escribir 'dos'
			SiNo
				Si a>=3 Y a<=5 Entonces
					Escribir 'entre 3 y 5'
				SiNo
					// de otro modo
					Escribir 'otro valor'
				FinSi
			FinSi
		FinSi
	FinSi
FinAlgoritmo
