Algoritmo incluido
	// Cargar un vector/arreglo de 100 elementos con numeros aleatorios entre 0-10
	// a) Ingresar un numero e indicar si el numero esta incluido en el vector
	// b) Indicar cuantas veces aparece el numero en el arreglo
	
	dimension vector[100]
	
	// cargar vector
	para i=1 hasta 100 Hacer
		vector[i] = Aleatorio(0,10)
	FinPara
	
	escribir 'Ingrese un numero entre 0-10:'
	leer num
	
	//A)
	se_encontro=0 //bandera
	para i=1 hasta 100 Hacer
		si vector[i] == num Entonces
			se_encontro = 1
		FinSi
	FinPara
	si se_encontro==1
		escribir 'El numero ' num ' esta en el vector'
	SiNo
		escribir 'El numero ' num ' no esta en el vector'
	FinSi
	
	//B)
	contador=0
	para i=1 hasta 100 Hacer
		si vector[i] == num Entonces
			contador = contador + 1
		FinSi
	FinPara
	si contador>0
		escribir 'El numero ' num ' aparecio ' contador ' veces'
	SiNo
		escribir 'El numero ' num ' no esta en el vector'
	FinSi
	
FinAlgoritmo
