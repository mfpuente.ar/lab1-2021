# Diccionario = { llave : valor }

dic = {'uno': 1, 'dos': 2, 'tres': 4}

# Mostrar todo el diccionario
print(dic)

# Leer un elemento
llave = 'uno'
print(dic[llave], dic['dos'])

# Escribir un elemento del diccionario
dic['tres'] = 3
print(dic['tres'])

# Agregar un elemento al diccionario
dic['cuatro'] = 4
print(dic)