frase = 'una frase muy larga'
#frase = [1, 2, 3, 4, 5, 2, 3, 4, 23, 4, 2, 1, 3, 4, 1, 2, 4, 2, 34, 12, 31, 4, 14, 14]

# Creo un diccionario vacio
dic = dict()

for letra in frase:
    if letra not in dic:
        # Si no existe el caracter en el diccionario, lo creo con valor 1
        dic[letra] = 0 + 1
    else:
        # Si existe el caracter en el diccionario, lo incremento
        dic[letra] = dic[letra] + 1

print(dic)

