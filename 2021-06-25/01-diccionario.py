# Diccionario = { llave : valor }

dic = {0: 'cero', 1: 'uno', 2: 'tres'}

# Mostrar todo el diccionario
print(dic)

# Leer un elemento
llave = 0
print(dic[llave], dic[1])

# Escribir un elemento del diccionario
dic[2] = 'dos'
print(dic[2])

# Agregar un elemento al diccionario
dic[3] = 'tres'
print(dic)