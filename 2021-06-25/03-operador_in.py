cadena = 'una cadena'

if 'd' in cadena:
    print('"d" Si esta en la cadena')
else:
    print('"d" No esta en la cadena')

if 'x' not in cadena:
    print('"x" no esta en la cadena')


lista = [4, 2, 5, 6]
if 4 in lista:
    print('4 Si esta en la lista')
else:
    print('4 no esta en la lista')


dic = {'uno': 1, 'dos': 2, 'tres': 3}
if 'dos' in dic:
    print('"dos" si esta en el diccionario')
else:
    print('"dos" no esta en el diccionario')
    
if 3 in dic.values():
    print('si esta en los valores del diccionario')