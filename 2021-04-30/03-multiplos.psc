Algoritmo sin_titulo
	// Ingresar 1 numero e indicar cuales son multiplos de 7
	// Por ejemplo, si ingreso 100, mostrar los multiplos de 7 entre 1 y 100.
	// Mensaje de ejemplo: 
	// 7 es multiplo de 7
	// 14 es multiplo de 7
	// ...
	// 98 es multiplo de 7
	
	// Alternativa
	escribir 'Ingrese un numero'
	leer n
	para i=7 hasta n con paso 7 Hacer
		escribir i ' es multiplo de 7'
	FinPara
FinAlgoritmo
