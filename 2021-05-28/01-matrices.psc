Algoritmo sin_titulo
	dimension m(999, 3)
	i = 1
	salir = falso
	Repetir
		escribir sin saltar 'Ingrese fecha: '
		leer fecha
		si fecha = "0" Entonces
			salir = verdadero
		SiNo
			m(i,1) = fecha
			escribir sin saltar 'Ingrese moneda base: '
			leer m(i,2) //moneda base
			escribir sin saltar 'Ingrese moneda a convertir: '
			leer m(i,3) //moneda a convertir
			i = i + 1
		FinSi
	Hasta Que salir = verdadero o i = 999
	
	//Mostrar matriz
	para k=1 hasta i
		para j=1 hasta 3
			escribir Sin Saltar m(k,j) ' '
		FinPara
		escribir ''
	FinPara
FinAlgoritmo
