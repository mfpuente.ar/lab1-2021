# defino funcion
def suma(x, y):
    z = x + y
    return z

a = 10
b = 5

c = suma(a, b)
d = suma(3, 5)

print('El resultado es:', c)


# Funcion con parametro opcional
def resta(a, b=1):
    return a - b

print(resta(10, 3))
print(resta(5))
print(resta(b=4, a=2))