texto = '   Un Texto En Minuscula   '

print(texto)

# Mayusculas
print(texto.upper())

# Minusculas
print(texto.lower())

# Contar
print(texto.count(' '))

# Dividir
print(texto.split())

# Quitar
print(texto.strip())