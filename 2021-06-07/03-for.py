# range(valor inferior, valor superior + 1, paso)

for i in range(1, 11, 2):
    print(i)


palabra = 'hola'
print(len(palabra))
for c in range(0, len(palabra)):
    print(palabra[c])

for c in 'hola':
    print(c)


arreglo = ['manzana', 'pera', 'banana']
for i in arreglo:
    print(i)
    print(len(i))

for i in range(0, len(arreglo)):
    print(arreglo[i])
