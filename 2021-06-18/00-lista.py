# Crear lista
lista = [1, 3, 5, 2, 5]

# Mostrar lista completa
print(lista)

# Mostrar item 1 de la lista
print(lista[1])

# Modificar un item de la lista
lista[1] = -7
print(lista[1])

# Rebanado de la lista
print(lista[1:3])

# Invertir lista
print(lista[::-1])
