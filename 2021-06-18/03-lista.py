# Funciones

lista = [1, 3, -5, 2, 5, 1]

# suma
suma = sum(lista)
print('La suma es:', suma)

# mayor
mayor = max(lista)
print('El mayor es:', mayor)

# menor
menor = min(lista)
print('El menor es:', menor)

# promedio
promedio = sum(lista) / len(lista)
#print('El promedio es: %.2f' % (promedio))
print('El promedio es:', round(promedio, 4))
