Algoritmo arreglo_ejemplo
	// Crear arreglo
	dimension arreglo[5]
	
	// cargar arreglo
	para i=1 hasta 5 Hacer
		leer arreglo[i]
	FinPara
	
	// mostrar arreglo
	para i=1 hasta 5 Hacer
		escribir 'arreglo[', i, ']=', arreglo[i]
	FinPara
FinAlgoritmo
