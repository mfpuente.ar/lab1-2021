Algoritmo ordenar
	dimension v[5]
	
	// Cargar
	v[1] = 13
	v[2] = 11
	v[3] = 42
	v[4] = 15
	v[5] = 10
	
	// Ordenar
	para i=1 hasta 4
		para j=1 hasta 5-i
			si (v[j] > v[j+1]) Entonces
				aux = v[j]
				v[j] = v[j+1]
				v[j+1] = aux
			FinSi
		FinPara
	FinPara
	
	// Mostrar
	para i=1 hasta 5
		escribir v[i]
	FinPara
FinAlgoritmo
