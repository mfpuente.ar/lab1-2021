Algoritmo ordenar
	dimension v[5]
	
	// Cargar
	v[1] = 13
	v[2] = 11
	v[3] = 42
	v[4] = 15
	v[5] = 10
	
	// Ordenar
	para i=1 hasta 4
		para j=i+1 hasta 5
			si (v[i] > v[j]) Entonces
				aux = v[i]
				v[i] = v[j]
				v[j] = aux
			FinSi
		FinPara
	FinPara
	
	// Mostrar
	para i=1 hasta 5
		escribir v[i]
	FinPara
FinAlgoritmo
